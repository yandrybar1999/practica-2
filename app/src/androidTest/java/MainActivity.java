import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practica5.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialoglogin = new Dialog(MainActivity.this);
                dialoglogin.setContentView(R.layout.dlg_login);
                EditText usuario = findViewById(R.id.txtUser);
                EditText clave = findViewById(R.id.txtPasswd);
                Toast.makeText(MainActivity.this, "Usuario: " + usuario.getText().toString() + " \n " + "Clave: " + clave.getText().toString(), Toast.LENGTH_SHORT).show();
                dialoglogin.show();

                break;
            case R.id.opcionRegistrar:
                Dialog dialogregistro = new Dialog(MainActivity.this);
                dialogregistro.setContentView(R.layout.dlg_registro);
                EditText name = findViewById(R.id.nombre);
                EditText apellido = findViewById(R.id.apellido);
                EditText passwd = findViewById(R.id.passwd);
                EditText passwd2 = findViewById(R.id.passwd2);
                Toast.makeText(MainActivity.this, "nombre: " + name.getText().toString() + " \n" +
                        "\n" + "apellido: " + apellido.getText().toString() + " \n " +
                        "password: " + passwd.getText().toString() + " \n " + "password_confirm: "
                        + passwd2.getText().toString(), Toast.LENGTH_SHORT).show();
                dialogregistro.show();
                break;
        }
        return true;
    }
}
