package com.example.practica5;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialoglogin = new Dialog(MainActivity.this);
                dialoglogin.setContentView(R.layout.dlg_login);
                EditText usuario = dialoglogin.findViewById(R.id.txtUser);
                EditText clave = dialoglogin.findViewById(R.id.txtPasswd);
                Toast.makeText(MainActivity.this, "Usuario: " + usuario.getText().toString() + " \n " + "Clave: " + clave.getText().toString(), Toast.LENGTH_SHORT).show();
                dialoglogin.show();

                break;
            case R.id.opcionRegistrar:
                Dialog dialogregistro = new Dialog(MainActivity.this);
                dialogregistro.setContentView(R.layout.dlg_registro);
                EditText name = dialogregistro.findViewById(R.id.nombre);
                EditText passwd = dialogregistro.findViewById(R.id.passwd);
                EditText passwd2 = dialogregistro.findViewById(R.id.passwd2);
                Toast.makeText(MainActivity.this, "su registro a sido exitoso: " + name.getText().toString() , Toast.LENGTH_SHORT).show();
                dialogregistro.show();
                break;
        }
        return true;
    }
}
